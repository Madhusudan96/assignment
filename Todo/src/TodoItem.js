import React,{ Component } from "react";
import './App.css';


const TodoItem=(props)=>{
	return (
	<div className="wrapper">
		<br />
		<div className="container">
			<span classname="item-name">{props.itemname}</span>
			<button type = "submit" className="todo-item" onClick={() => props.editItem(props.itemname,props.position)}>Change</button>
			<button type = "submit" className="todo-item" onClick={() => props.removeItem(props.position)}>Remove</button>
		</div>
	</div>
	);
}

export default TodoItem;