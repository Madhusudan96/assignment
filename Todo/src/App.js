import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import TodoItem from "./TodoItem"

class App extends Component{
  constructor(){
    super()
    this.additem = this.additem.bind(this);
  }
   state = {
    items: []
   }
   additem(e){
    if(e.key === "Enter" && e.target.value !== ""){
      this.setState({
        items: [...this.state.items,e.target.value]
      })
      e.target.value = "";
  }
}
edit=(name,id) => {
  const promtUser = prompt(name);
  if (promtUser){
    let copyItems = this.state.items;
    copyItems[id] = promtUser;
    this.setState({
      items:copyItems
    });
  }
  else{ 
    this.setState({
      items:this.state.items
    });
  }
  }


remove=(id)=>{
       const copyItems = this.state.items;
       copyItems.splice(id,1);
      this.setState({
          items:[...copyItems]
      });
      this.state.items.splice(id,1);  
    }
  

  render(){
   return (
    <div className="wrapper">
      <h1>ToDo-List({this.state.items.length})</h1>
      <input type = "text" size = "45" onKeyDown = {this.additem}/>
      {
        this.state.items.map((item,id)=>{
          return(
            <TodoItem itemname = {item} removeItem = {this.remove} editItem = {this.edit} position = {id}/>
          );
        })
      }
    </div>
  );
}
}

export default App;